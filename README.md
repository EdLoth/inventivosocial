# Site Inventivo Social
<h3>Plataforma Web criada com o fim de Auxiliar no alcance dos trabalhos da instituição <b>"Inventivo Social"</b>, com React JS, BootStrap.</h3>

<img src="https://i.ibb.co/nnFP87b/Capturar.png" >


:white_check_mark: **Tecnologias**

- <h4>React</h4>
- <h4>BootStrap</h4>


:white_check_mark: **Freameworks:**

- <h4>BootStrap</h4>
- <h4>YARN</h4>
- <h4>NPM</h4>

<br><br>


**<h1>Autor:</h1>**
<img src="https://avatars0.githubusercontent.com/u/57899212?s=400&u=fe2c12114ceb4e17feffe2a50894d6c83531e087&v=4" width="200px"  ><br>
- **Nome**
Eduardo Silveira Ramos<br>
- **GitHub**
[EdLoth](https://github.com/EdLoth)<br>
- **Perfil Linkedin**
[Linkedin](https://www.linkedin.com/in/eduardo-ramos-31413b1a2/)<br>
- **Email Contato**
contatoeduardo.dev@gmail.com<br><br>


:clipboard:**Comandos**

* Instalar React-App

`npm install -g create-react-app`

* Inicializar um App

`create-react-app minha-primeira-app`


**-----------------------------**

**Metas**
- [x] Front-End Pronto
- [x] Aplicação Hospedada
- [x] Dominio Adquirido
- [ ] Integrar Api Rest-Full
- [ ] Desenvolver App Mobile
