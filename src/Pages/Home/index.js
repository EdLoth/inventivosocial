import React from 'react';
import NavBar from '../../Components/NavBar'
import Footer from '../../Components/Footer'
import './styles.css';

import edu from './assets/Team/a.jpg';
import duda from './assets/Team/duda.jpg';
import nanda from './assets/Team/nanda.jpg';
import rafa from './assets/Team/rafa.jpg';

import whapp from './assets/whatsapp.png'
import { Link } from 'react-router-dom';

export default function Home() {
    return (
        <div>
            

            <div class="whapp">
                <a href="https://api.whatsapp.com/send?l=pt&amp;phone=5541999999">
                    <img src={whapp} style={{ height: '80px', position: 'fixed', bottom: '25px', right: '25px', zIndex: 999999 }} data-selector="img" /></a>
            </div>
            <NavBar />

            <header>
                <div className="contentTop"></div>

                
            </header>


            <div className="contentSection">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                        <section id="servicos"></section>
                            <center><p id="titleServices">SERVICES</p></center>
                            <div className="container">
                                <div className="row">
                                    <div className="col-6">
                                        <p id="subtitleSction">Title 1</p>
                                        <div className="icone1">
                                        <p class="animate__animated animate__bounce animate__delay-2s">
                                            Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                            tipográfica e de impressos, e vem sendo utilizado desde o
                                            século XVI, quando um impressor desconhecido pegou uma
                                            bandeja de tipos e os embaralhou para fazer um livro
                                            de modelos de tipos. Lorem Ipsum sobreviveu não só a
                                            cinco séculos, como também ao salto para a editoração
                                            eletrônica, permanecendo essencialmente inalterado.
                                            Se popularizou na década de 60, quando a Letraset
                                            lançou decalques contendo passagens de Lorem Ipsum,
                                            e mais recentemente quando passou a ser integrado a
                                            softwares de editoração eletrônica como Aldus PageMaker.
                                        </p>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <p id="subtitleSction">Title 2</p>
                                        <p>
                                            Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                            tipográfica e de impressos, e vem sendo utilizado desde o
                                            século XVI, quando um impressor desconhecido pegou uma
                                            bandeja de tipos e os embaralhou para fazer um livro
                                            de modelos de tipos. Lorem Ipsum sobreviveu não só a
                                            cinco séculos, como também ao salto para a editoração
                                            eletrônica, permanecendo essencialmente inalterado.
                                            Se popularizou na década de 60, quando a Letraset
                                            lançou decalques contendo passagens de Lorem Ipsum,
                                            e mais recentemente quando passou a ser integrado a
                                            softwares de editoração eletrônica como Aldus PageMaker.
                                        </p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <p id="subtitleSction">Title 3</p>
                                        <div className="icone1">
                                        <p class="animate__animated animate__bounce animate__delay-2s">
                                            Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                            tipográfica e de impressos, e vem sendo utilizado desde o
                                            século XVI, quando um impressor desconhecido pegou uma
                                            bandeja de tipos e os embaralhou para fazer um livro
                                            de modelos de tipos. Lorem Ipsum sobreviveu não só a
                                            cinco séculos, como também ao salto para a editoração
                                            eletrônica, permanecendo essencialmente inalterado.
                                            Se popularizou na década de 60, quando a Letraset
                                            lançou decalques contendo passagens de Lorem Ipsum,
                                            e mais recentemente quando passou a ser integrado a
                                            softwares de editoração eletrônica como Aldus PageMaker.
                                        </p>
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <p id="subtitleSction">Title 4</p>
                                        <p>
                                            Lorem Ipsum é simplesmente uma simulação de texto da indústria
                                            tipográfica e de impressos, e vem sendo utilizado desde o
                                            século XVI, quando um impressor desconhecido pegou uma
                                            bandeja de tipos e os embaralhou para fazer um livro
                                            de modelos de tipos. Lorem Ipsum sobreviveu não só a
                                            cinco séculos, como também ao salto para a editoração
                                            eletrônica, permanecendo essencialmente inalterado.
                                            Se popularizou na década de 60, quando a Letraset
                                            lançou decalques contendo passagens de Lorem Ipsum,
                                            e mais recentemente quando passou a ser integrado a
                                            softwares de editoração eletrônica como Aldus PageMaker.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className="intermed1">
                <div className="row">
                    <div className="col-12">
                        <div className="banner">
                        </div>
                        <div className="contentIntermed1">
                            <form>
                                <p id="workIntermed">Come be a Volunteer:</p>
                                <p id="titleForm">Register:</p>
            
                                <div className="inputContentIntermed">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-user-circle-o" ></i></span>
                                        </div>
                                        <input type="text" id="inputIntermed" placeholder="Full Name" aria-describedby="basic-addon1"/>
                                    </div>
                                </div>

                                <div className="inputContentIntermed">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-address-card-o" ></i></span>
                                        </div>
                                        <input type="text" id="inputIntermed" placeholder="Email" aria-describedby="basic-addon1"/>
                                    </div>
                                </div>

                                <div className="inputContentIntermed">
                                    <div id="iptGroup" class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-child" aria-hidden="true"></i></span>
                                        </div>
                                        <input type="text" id="inputIntermed" placeholder="What makes you inventive:" aria-describedby="basic-addon1"/>
                                    </div>
                                </div>
                                <button id="btnForm" type="submit">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div className="teamContent">
                <section id="team" class="pb-5">
                    <div class="container">
                        <h5 class="section-title h1">Our Team</h5>
                        <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="image-flip" >
                                    <div class="mainflip flip-0">
                                        <div class="frontside">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <p><img class=" img-fluid" src={rafa} alt="card image" /></p>
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="card-text">Professional's role.</p>
                                                    <a href="https://www.fiverr.com/share/qb8D02" class="btnTeam"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="backside">
                                            <div class="card">
                                                <div class="card-body text-center mt-4">
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="texTeam">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-skype"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                    <div class="mainflip">
                                        <div class="frontside">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <p><img class=" img-fluid" src={nanda} alt="card image" /></p>
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="card-text">Professional's role.</p>
                                                    <a href="https://www.fiverr.com/share/qb8D02" class="btnTeam"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="backside">
                                            <div class="card">
                                                <div class="card-body text-center mt-4">
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="texTeam">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-skype"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                    <div class="mainflip">
                                        <div class="frontside">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <p><img class=" img-fluid" src={duda} alt="card image" /></p>
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="card-text">Professional's role.</p>
                                                    <a href="https://www.fiverr.com/share/qb8D02" class="btnTeam"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="backside">
                                            <div class="card">
                                                <div class="card-body text-center mt-4">
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="texTeam">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-skype"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                    <div class="mainflip">
                                        <div class="frontside">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <p><img class=" img-fluid" src={edu} alt="card image" /></p>
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="card-text">Professional's role.</p>
                                                    <a href="https://www.fiverr.com/share/qb8D02" class="btnTeam"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="backside">
                                            <div class="card">
                                                <div class="card-body text-center mt-4">
                                                    <h4 class="card-title">Profissional Name</h4>
                                                    <p class="texTeam">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="https://www.fiverr.com/share/qb8D02">
                                                                <i class="fa fa-skype"></i>
                                                            </a>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <a class="social-icon text-xs-center" target="_blank" href="eduardosram10@gmail.com">
                                                                <i class="fa fa-google"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
       

            <div className="intermed2">
            <div className="row">
                    <div className="col-12">
                        <div className="contentInter2">
                            <Link>
                            <p id="workIntermed2">BE THE DIFFERENCE, BE INVENTIVE!</p>
                            </Link>
                            
                        </div>
                    </div>
                </div>
            </div>
       <Footer />
        </div>
    )
}
