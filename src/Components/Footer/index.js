import React from 'react'
import './styles.css'

export default function Footer() {
    return (
        <div>
            <footer class="new_footer_area bg_color">
                <div class="new_footer_top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="f_widget company_widget wow fadeInLeft" data-wow-delay="0.2s" style={{visibility:'visible', animationDelay:'0.2s', animationName:'fadeInLeft'}}>
                                    <h3 class="f-title f_600 t_color f_size_18">Novidades:</h3>
                                    <p>Se inscreva e seja o primeiro a receber as novidades!</p>
                                    <form action="#" class="f_subscribe_two mailchimp" method="post" novalidate="true" _lpchecked="1">
                                        <input type="text" name="EMAIL" class="form-control memail" placeholder="Email" />
                                            <button class="btn btn_get btn_get_two" type="submit">Inscreva-se</button>
                                            <p class="mchimp-errmessage" style={{display:'none'}}></p>
                                            <p class="mchimp-sucmessage" style={{display:'none'}}></p>
                                </form>
                            </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.4s" style={{visibility:'visible', animationDelay:'0.4s', animationName:'fadeInLeft'}}>
                                        <h3 class="f-title f_600 t_color f_size_18">Download</h3>
                                        <ul class="list-unstyled f_list">
                                            <li><a href="#">Company</a></li>
                                            <li><a href="#">App Android</a></li>
                                            <li><a href="#">App IOS</a></li>
                                            <li><a href="#">Nossas INVENTIVIDADES</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.6s" style={{visibility:'visible', animationDelay:'0.6s', animationName:'fadeInLeft'}}>
                                        <h3 class="f-title f_600 t_color f_size_18">Ajuda</h3>
                                        <ul class="list-unstyled f_list">
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Termos de Condições</a></li>
                                            <li><a href="#">Documentação</a></li>
                                            <li><a href="#">Privacidade</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="f_widget social-widget pl_70 wow fadeInLeft" data-wow-delay="0.8s" style={{visibility:'visible', animationDelay:'0.8s', animationName:'fadeInLeft'}}>
                                        <h3 class="f-title f_600 t_color f_size_18">Team Solutions</h3>
                                        <div class="f_social_icon">
                                            <a href="#" class="fab fa-facebook"></a>
                                            <a href="#" class="fab fa-twitter"></a>
                                            <a href="#" class="fab fa-linkedin"></a>
                                            <a href="#" class="fab fa-pinterest"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer_bg">
                            <div class="footer_bg_one"></div>
                            <div class="footer_bg_two"></div>
                            <div class="footer_bg_tree"></div>
                        </div>
                    </div>
                    <div class="footer_bottom">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-sm-7">
                                    <p class="mb-0 f_400">© Inventivo Social Inc.. 2020 All rights reserved.</p>
                                </div>
                                <div class="col-lg-6 col-sm-5 text-right">
                                <div class="f_social_icon">
                                            <a href="#" class="fab fa-github"></a>
                                            <a href="#" class="fab fa-gitlab"></a>
                                            <a href="#" class="fab fa-linkedin"></a>
                                            <a href="#" class="fab fa-whatsapp"></a>
                                        </div>
                                    <p>Developed by Eduardo Ramos <a href="#">CTO - Inventivo Social</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
        </footer>
        </div>
    )
}
