import React from 'react';
import './styles.css'
import { Link } from 'react-router-dom'



export default function NavBar() {
    return (

        <div className="NavContainer">

            <nav class="navbar navbar-expand-lg navbar-dark gris scrolling-navbar fixed-top text-center">
                <a class="navbar-brand" id="ap" href="#"><div id="ani">Social Inventive <i class="fa fa-heart"></i></div></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navContent">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" id="nameNav" href="#home">HOME <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="nameNav" href="#servicos">SERVICES</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="nameNav" href="#workIntermed-team">VOLUNTER</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="nameNav" href="#team">TEAM</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}
